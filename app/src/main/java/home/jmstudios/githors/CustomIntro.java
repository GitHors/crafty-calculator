package home.jmstudios.githors;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro2;

/**
 * Class created by Artem for custom introdution screen.
 */
public class CustomIntro extends AppIntro2 {

    @Override
    public void init(Bundle savedInstanceState) {
        // Здесь указываем количество слайдов, например нам нужно 3
        addSlide(SampleSlide.newInstance(R.layout.intro_1)); //
        addSlide(SampleSlide.newInstance(R.layout.intro_2));

    }

    private void loadMainActivity(){
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
    }

    @Override
    public void onNextPressed() {
    }

    @Override
    public void onDonePressed() {
        finish();
    }

    @Override
    public void onSlideChanged() {
    }
}
